package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.Project;
import ru.vlasova.taskmanager.model.Task;
import ru.vlasova.taskmanager.repository.ProjectRepository;

import java.util.Date;
import java.util.List;

@Service("projectService")
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    @Qualifier(value = "taskService")
    private ITaskService taskService;

    @Autowired
    @Qualifier(value = "projectRepository")
    private ProjectRepository repository;

    @Override
    @Nullable
    public Project insert(@Nullable final String name,
                          @Nullable final String description, @Nullable final Date dateStart,
                          @Nullable final Date dateFinish, @Nullable final String status) {
        final boolean checkGeneral = isValid(name, description);
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(Status.valueOf(status));
        return project;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    @Nullable
    public Project getProjectByIndex(final int index) {
        if (index < 0) return null;
        @NotNull final List<Project> projectList = (List<Project>) repository.findAll();
        return projectList.get(index);
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectIndex(final int projectIndex) {
        if (projectIndex < 0) return null;
        @NotNull final List<Project> projectList = (List<Project>) repository.findAll();
        @Nullable final String projectId = projectList.get(projectIndex).getId();
        return taskService.getTasksByProjectId(projectId);
    }

    @Override
    @Nullable
    public List<Project> search(@Nullable final String searchString) {
        if (searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final List<Project> projectList = repository.
                findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(searchString, searchString);
        return projectList;
    }


    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final List<Project> projectList = (List<Project>) repository.findAll();
        return projectList;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Project project = repository.findById(id).orElse(null);
        return project;
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @NotNull
    public List<Project> sortProject(@Nullable final String sortMode) {
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return repository.findAllByOrderByDateCreate();
                case ("2"):
                    return repository.findAllByOrderByDateStart();
                case ("3"):
                    return repository.findAllByOrderByDateFinish();
                case ("4"):
                    return repository.findAllByOrderByStatusAsc();
            }
        }
        return repository.findAllByOrderByNameAsc();
    }

}