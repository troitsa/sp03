package ru.vlasova.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.Project;
import ru.vlasova.taskmanager.model.Task;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @RequestMapping("/task_list")
    public ModelAndView taskList() {
        @Nullable final List<Task> taskList = taskService.findAll();
        @NotNull final ModelAndView mav = new ModelAndView("task_list");
        mav.addObject("taskList", taskList);
        return mav;
    }

    @RequestMapping("/new_task")
    public String newTaskForm(@NotNull final Map<String, Object> model) {
        @NotNull final Task task = new Task();
        @Nullable final List<Project> projectList = projectService.findAll();
        model.put("task", task);
        model.put("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        model.put("statusList", statusList);
        return "new_task";
    }

    @RequestMapping(value = "task_save", method = RequestMethod.POST)
    public String saveTask(@Nullable @RequestParam(required = false) final String id,
                           @Nullable @RequestParam final String name,
                           @Nullable @RequestParam final String description,
                           @Nullable @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") final Date dateStart,
                           @Nullable @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") final Date dateFinish,
                           @Nullable @RequestParam final String project,
                           @Nullable @RequestParam final String status) {
        @Nullable final Task task = taskService.insert(name, description, dateStart, dateFinish, project, status);
        if (id != null) {
            task.setId(id);
        }
        taskService.merge(task);
        return "redirect:/task_list";
    }

    @RequestMapping("/edit_task")
    public ModelAndView editTaskForm(@NotNull @RequestParam final String id) {
        ModelAndView mav = new ModelAndView("edit_task");
        @Nullable final Task task = taskService.findOne(id);
        @Nullable final List<Project> projectList = projectService.findAll();
        mav.addObject("task", task);
        mav.addObject("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        mav.addObject("statusList", statusList);
        return mav;
    }

    @RequestMapping("/delete_task")
    public String deleteTaskForm(@NotNull @RequestParam final String id) {
        taskService.remove(id);
        return "redirect:/task_list";
    }

    @RequestMapping("search_task")
    public ModelAndView search(@NotNull @RequestParam final String keyword) {
        @Nullable final List<Task> result = taskService.search(keyword);
        @NotNull final ModelAndView mav = new ModelAndView("search_task");
        mav.addObject("result", result);
        return mav;
    }

}
