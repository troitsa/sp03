package ru.vlasova.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.Project;
import ru.vlasova.taskmanager.model.Task;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project insert(@Nullable final String name,
                   @Nullable final String description, @Nullable final Date dateStart,
                   @Nullable final Date dateFinish, @Nullable final String status);

    @Nullable
    Project getProjectByIndex(int index);

    void remove(@Nullable final String id);

    @Nullable
    List<Task> getTasksByProjectIndex(int projectIndex);

    @Nullable
    List<Project> search(@Nullable final String searchString);

    @Nullable
    List<Project> findAll();

    @Nullable
    Project findOne(@Nullable final String id);

    void persist(@Nullable final Project obj);

    void merge(@Nullable final Project obj);

    void removeAll();

    @Nullable
    List<Project> sortProject(@Nullable final String sortMode);

}