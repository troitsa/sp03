package ru.vlasova.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task insert(@Nullable final String name,
                @Nullable final String description, @Nullable final Date dateStart,
                @Nullable final Date dateFinish, @Nullable final String project,
                @Nullable final String status);

    @Nullable
    List<Task> getTasksByProjectId(@Nullable final String projectId);

    void remove(@Nullable final String id);

    @Nullable
    List<Task> search(@Nullable final String searchString);

    @Nullable
    List<Task> findAll();

    @Nullable
    Task findOne(@Nullable final String id);

    void persist(@Nullable final Task obj);

    void merge(@Nullable final Task obj);

    void removeAll();

    @Nullable
    List<Task> sortTask(@Nullable final String sortMode);

}
